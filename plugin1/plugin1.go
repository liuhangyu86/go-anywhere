package main

import (
	"fmt"

	"github.com/Shopify/sarama"
	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
)

// import (
// 	"fmt"
// )

// type plugin1 struct {
// }

// func (p *plugin1) Run() {
// 	fmt.Println()
// }

type Job struct {
	DB     *sqlx.DB
	KfkPro sarama.SyncProducer
	KfkCon sarama.Consumer
	RedCli *redis.Client
}

func (job *Job) Set(db *sqlx.DB, pro sarama.SyncProducer, cons sarama.Consumer, redCli *redis.Client) {
	job.DB = db
	job.KfkPro = pro
	job.KfkCon = cons
	job.RedCli = redCli
}

func (job *Job) Run() {

}

var JobIns Job

//package main
func Add(x, y int) int {
	return x + y
}
func Subtract(x, y int) int {
	return x - y
}

func main() {
	fmt.Println("sss")
}
