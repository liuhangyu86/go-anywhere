package service

import (
	"github.com/robfig/cron"
)

type Jobs struct {
	Name    string
	Spec    string
	StartUp bool
}

type CronSrv struct {
	InsCron *cron.Cron
	JobsMap map[cron.Job]Jobs
}

func NewCronSrv() *CronSrv {
	cronSrv := new(CronSrv)
	cronSrv.InsCron = cron.New()
	cronSrv.JobsMap = make(map[cron.Job]Jobs)
	return cronSrv
}

func (cs *CronSrv) RegisterJob(job interface{}, name string, spec string, startup bool) {
	if cs == nil {
		return
	}

	if jobIns, ok := job.(*cron.Cron); ok {
		cs.JobsMap[jobIns] = Jobs{Name: name, Spec: spec, StartUp: startup}
	}

}

func (cs *CronSrv) CronSrvStart() error {
	if cs == nil {
		return nil
	}

	for jobIns, jobOpt := range cs.JobsMap {
		if job, ok := jobIns.(*cron.Cron); ok {
			err := cs.InsCron.AddJob(jobOpt.Spec, job)
			if err != nil {
				return err
			}

			if jobOpt.StartUp {
				//ins := jobIns.(cron.Cron)
			}
		}
	}
	cs.InsCron.Start()
	return nil
}
