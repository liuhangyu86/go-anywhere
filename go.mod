module go-anywhere

go 1.12

require (
	github.com/Shopify/sarama v1.22.1
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/robfig/cron v1.1.0
	google.golang.org/appengine v1.6.0 // indirect
)
