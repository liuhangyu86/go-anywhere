package api

type Interface interface {
	RegisterJob(job interface{}, spec string, startup bool) error
}
